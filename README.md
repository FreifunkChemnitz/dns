<!-- SPDX-FileCopyrightText: 2021 Freifunk Chemnitz e.V. -->
<!-- SPDX-License-Identifier: Unlicense -->

# DNS

This repository contains the zone files used by Freifunk Chemnitz e.V.
